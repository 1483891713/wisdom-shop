import { createStore } from 'vuex';
import user from './modules/user';
import home from './modules/home';
import cate from './modules/cate';
import search from './modules/search';
import detail from './modules/detail';
import getters from './getters';
import cart from './modules/cart';

const store = createStore({
	modules: {
		user,
		home,
		cate,
		search,
		detail,
		cart
	},
	getters
});

export default store;
