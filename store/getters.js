const getters = {
	goodsInfo: (state) => state.detail.goodsInfo,
	serveList: (state) => state.detail.serveList,
	evaluateList: (state) => state.detail.evaluateList,
	cartList: (state) => state.cart.cartList
};
export default getters;
