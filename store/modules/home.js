import { indexApi } from '@/apis/index';
import { HOME_BANNER } from '@/constant/home.js';

const SUFFIX_INFO = 'Info'; //后缀

const home = {
	namespaced: true,
	state: {},
	mutations: {
		setData(state, data) {
			const { type, value } = data;

			state[type + SUFFIX_INFO] = value;
		}
	},
	actions: {
		async fetchPageData(context) {
			const { data, status } = await indexApi();
			if (status !== 200) uni.utils.toast('获取上首页数据失败');

			const items = data.pageData.items;
			// console.log(items);
			items.forEach((item) => {
				if (Array.isArray(item.data)) {
					context.commit('setData', { type: item.type, value: item.data });
				} else {
					context.commit('setData', { type: item.type, value: item.params });
				}
			});
		}
	}
};

export default home;
