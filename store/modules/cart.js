import { cartListApi, cartUpdateApi, delCartApi, checkOutOrederApi } from '@/apis/cart';
import { CARTTOTAL_NUMBER } from '@/constant/common';
import { setCache, getCache } from '@/utils/cache';
import { getTabBarBadgeFn } from '@/utils/TabBarBadge';
const cart = {
	namespaced: true,
	state: () => {
		return {
			cartList: [] //数据渲染的数据
		};
	},
	getters: {
		getTotalPrice(state) {
			return state.cartList
				?.filter((item) => item.isSelected)
				.reduce((prev, item) => prev + item.num * item.price, 0);
		},
		getSelectNumber(state) {
			return state.cartList.filter((item) => item.isSelected).length;
		},
		getCartTotal(state) {
			const cartTotal = state.cartList.reduce((prev, item) => prev + item.num, 0);
			setCache(CARTTOTAL_NUMBER, cartTotal);
			getTabBarBadgeFn(cartTotal);
			return cartTotal;
		},
		// filterSingleGoodsDelete(state) {
		// 	const goods = state.cartList?.find((item) => item.isSelected);

		// 	return;
		// },
		filterMoreGoodsDelete(state) {
			const goodsList = state.cartList?.filter((item) => item.isSelected);
			if (goodsList && goodsList.length > 0) {
				return goodsList.map((item) => item.id);
			}
			return [];
		},
		checkSelected(state) {
			return state.cartList.some((item) => item.isSelected);
		}
	},
	mutations: {
		setCartInfo(state, { cartList, cartTotal }) {
			if (cartTotal > 0) {
				state.cartList = cartList.map((item) => {
					return {
						id: item.id,
						goodsId: item.goods_id,
						price: item.goods.goods_price_min,
						goodsImg: item.goods.goods_image,
						goodsName: item.goods.goods_name,
						num: item.goods_num,
						isSelected: false
					};
				});
				// state.cartList = cartList;
				// state.cartTotal = cartTotal;
				setCache(CARTTOTAL_NUMBER, cartTotal);
				getTabBarBadgeFn(cartTotal);
			}
		},
		setItemSelected(state, value) {
			const { id, isSelected } = value;

			const goods = state.cartList.find((item) => item.id === id);
			goods.isSelected = isSelected;
		},
		setAllSelected(state, value) {
			state.cartList.forEach((item) => (item.isSelected = value));
		},

		updateGoodsNumer(state, value) {
			const { goodsId, num } = value;
			const goods = state.cartList.find((item) => item.goodsId === goodsId);
			goods.num = num;
		},

		deleteCartGoods(state, ids) {
			const goodsList = state.cartList.filter((item) => !ids.includes(item.id));
			state.cartList = goodsList;
		}
	},
	actions: {
		async fetchCartList(context) {
			console.log(context);
			const { data, status } = await cartListApi();
			if (status !== 200) return uni.utils.toast('获取购物车列表失败');

			context.commit('setCartInfo', { cartList: data.list, cartTotal: data.cartTotal });
		},

		async fetchUpdateCart(context, { goodsId, num }) {
			const { data, status } = await cartUpdateApi(goodsId, num);
			if (status !== 200) return uni.utils.toast('购物车商品更新失败');
			context.commit('updateGoodsNumer', { goodsId, num });
		},
		async fetchDeleteCartGoods(context) {
			const ids = context.getters['filterMoreGoodsDelete'];
			console.log(ids);
			if (ids && ids.length > 0) {
				const { status, data } = await delCartApi(ids);
				if (status !== 200) return uni.utils.toast('购物车商品删除失败');
				context.commit('deleteCartGoods', ids);

				uni.utils.toast('购物车商品删除成功');
			}
		},

		async fetchOrderAccount(context) {
			let cartIds = context.getters['filterMoreGoodsDelete'];
			if (cartIds && cartIds.length > 0) {
				cartIds = cartIds.join(',');
				const params = {
					mode: 'cart',
					delivery: 0,
					couponId: 0,
					isUsePoints: 0,
					cartIds
				};
				const { status, data } = await checkOutOrederApi(params);
				if (status !== 200) return uni.utils.toast('生成订单失败');
			}
			return '';
		}
	}
};
export default cart;
