import { goodsDetailApi, goodsServiceApi, goodsEvaluateApi } from '@/apis/detail';

const detail = {
	namespaced: true,
	state: () => {
		return {
			// 用户信息（登录成功存储）
			goodsInfo: {},
			serveList: [],
			evaluateList: []
		};
	},
	mutations: {
		setGoodsInfo(state, value) {
			state.goodsInfo = value;
		},
		setServeList(state, value) {
			state.serveList = value;
		},
		setEvaluateList(state, value) {
			state.evaluateList = value;
		}
	},
	actions: {
		//获取商品数据
		async fetchGoodsDetail(context, goodsId) {
			const { status, data } = await goodsDetailApi(goodsId);

			if (status !== 200) return uni.utils.toast('商品详情数据获取失败');

			context.commit('setGoodsInfo', data.detail);
		},

		// 获取产品服务列表
		async fetchGoodsService(context, goodsId) {
			const { status, data } = await goodsServiceApi(goodsId);
			if (status !== 200) return uni.utils.toast('获取商品服务失败');
			context.commit('setServeList', data.list);
		},
		//获取评论列表
		async fetchGoodsEvaluate(context, { goodsId, limit }) {
			const { status, data } = await goodsEvaluateApi(goodsId, limit);
			if (status !== 200) return uni.utils.toast('获取商品评价失败');
			context.commit('setEvaluateList', data.list);
		}
	}
};

export default detail;
