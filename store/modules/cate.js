import { cateApi } from '@/apis/cate';

const cate = {
	namespaced: true,
	state: () => {
		return {
			cateList: [],
			index: 0
		};
	},
	getters: {
		cateChildList(state) {
			return state.cateList[state.index]?.children;
		}
	},
	mutations: {
		//获取分类列表
		setCateList(state, value) {
			state.cateList = value;
		},
		//获取选择分类信息
		selectCateInfo(state, data) {
			const { id, index } = data;
			const flag = state.cateList.some((item) => item.category_id === id);
			if (!flag) return;
			state.index = index;
			// const cateInfo = state.cateList.find((item) => item.category_id === parentId);
			// state.cateChildList = cateInfo.children;
		}
	},
	actions: {
		//获取分类列表 请求接口
		async fetchCateAll(context) {
			const { status, data } = await cateApi();
			if (status !== 200) return uni.utils.toast('分类列表加载失败');
			context.commit('setCateList', data.list);
		}
	}
};

export default cate;
