import { SEARCH_RECORD } from '../../constant/common.js';
import { setCache, getCache, removeCache } from '@/utils/cache.js';
const search = {
	namespaced: true,
	state: () => {
		return {
			searchRecord: getCache(SEARCH_RECORD) || []
		};
	},
	getters: {},
	mutations: {
		setSearchRecord(state, value) {
			if (value.trim()) {
				state.searchRecord.push(value);

				setCache(SEARCH_RECORD, state.searchRecord);
			}
		},
		clearSearchRecord(state) {
			state.searchRecord = [];
			removeCache(SEARCH_RECORD);
		}
	},
	actions: {}
};

export default search;
