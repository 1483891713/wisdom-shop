/**
 * 存储登录相关用户数据
 */
import { USER_KEY } from '@/constant/login';
import { setCache, getCache, clearCache } from '@/utils/cache.js';
// 本地存储属性名
// const user_key = 'zh-user';
const user = {
	namespaced: true,
	state: () => {
		return {
			// 用户信息（登录成功存储）
			userInfo: getCache(USER_KEY) || ''
		};
	},
	mutations: {
		setUserInfo(state, value) {
			console.log(value);
			state.userInfo = value;
			setCache(USER_KEY, value);
		},
		clearUserInfo(state) {
			state.userInfo = '';
			clearCache();
		}
	},
	actions: {}
};

export default user;
