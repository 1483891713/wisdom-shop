/**
 * 通用校验数据
 * @param {*} value 值
 * @param {*} isRegRule  是否增加正则
 * @param {*} regRule  正则校验规则
 */
export function commonDataValid(value, isRegRule = false, regRule) {
	let regFlag = false;

	let isNullFlag = isCheckNull(value);
	if (isNullFlag && isRegRule) {
		regFlag = regCheck(value, regRule);
	}
	return {
		isNullFlag,
		regFlag
	};
}

function isCheckNull(value) {
	if (typeof value === 'string') {
		if (value.trim() === '') return false;
		return true;
	}
}

function regCheck(value, rule) {
	if (value.trim()) {
		return rule.test(value);
	} else {
		return false;
	}
}
