export const utils = {
	toast(title = '数据加载失败', duration = 1000, icon = 'none') {
		uni.showToast({
			title,
			icon,
			mask: true,
			duration
		});
	}
};

uni.utils = utils;
// utils
