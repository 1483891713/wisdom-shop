/**
 * 设置缓存
 * @param {*} key
 * @param {*} value
 */
export function setCache(key, value) {
	if (!value) return;

	uni.setStorageSync(key, value);
}

/**
 * 获取缓存
 * @param {*} key
 */
export function getCache(key) {
	return uni.getStorageSync(key);
}

/**
 * 移除指定缓存
 * @param {*} key
 */
export function removeCache(key) {
	const value = getCache(key);
	if (!value) return;
	uni.removeStorageSync(key);
}
/**
 * 清空缓存
 */
export function clearCache() {
	uni.clearStorageSync();
}
