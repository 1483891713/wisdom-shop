
/**
 * 提示消息封装
 * @param {*} promptMessage 提示相关信息
 * @param {*}  checkRule 校验规则
 */
export function messageToast(promptMessage, checkRule = true) {
  // if (typeof promptMessage !== "object") return false;
  let flag = true;
  if (checkRule) {
    wx.showToast({ ...promptMessage })
    flag = false
  }
  return flag;
}
export function commonMessageToast(promptMessage) {
  wx.showToast({ ...promptMessage })
}