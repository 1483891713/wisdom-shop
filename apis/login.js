import request from '@/utils/request.js';
import { IMAGE_CODE_H5 } from '@/constant/login.js';
/**
 * 手机号、验证码登录
 * @param formData:{mobile, smsCode=246810 验证码固定}
 */
export const loginApi = (formData) => {
	return request.post('/passport/login', {
		form: {
			...formData,
			// 先写死
			isParty: false,
			partyData: {}
		}
	});
};
/**
 * 生成图形验证码
 */
export const createImageApi = (platform = IMAGE_CODE_H5) => {
	return request.get('/captcha/image', {
		platform
	});
};

/**
 * 获取短信验证码
 */
export const createMessageCodeApi = (platform = IMAGE_CODE_H5) => {
	return request.get('/captcha/sendSmsCaptcha', {
		platform
	});
};
