import request from '@/utils/request.js'

/**
 * 获取登录用户信息
 */
export const userApi = () => {
	return request.get('/user/info')
}