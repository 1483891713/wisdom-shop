import request from '@/utils/request.js'

/**
 * 获取首页数据
 */
export const indexApi = () => {
	return request.get('page/detail', {
		pageId: 0
	})
}