import request from '@/utils/request.js';
import exp from 'constants';

/**
 * 获取当前登录用户的购物车列表
 * @returns
 */
export const cartListApi = () => {
	return request.get('cart/list');
};
/**
 *  删除购物车商品
 * @param {*} id 删除商品Id
 * @returns
 */
export const delCartApi = (cartIds) => {
	return request.post('/cart/clear', {
		cartIds
	});
};
/**
 * 修改购物车商品数量
 * @param {*} goodsId
 * @param {*} goodsNum
 * @returns
 */
export const cartUpdateApi = (goodsId, goodsNum) => {
	return request.post('/cart/update', {
		goodsId,
		goodsSkuId: '0',
		goodsNum
	});
};
/**
 * 添加商品到购物车
 */
export const cartAddApi = ({ goodsId, goodsNum }) => {
	return request.post('/cart/add', {
		goodsId,
		goodsSkuId: '0',
		goodsNum
	});
};

/**
 * 订单结算
 */
export const checkOutOrederApi = (params) => {
	return request.get('/checkout/order', {
		...params
	});
};
