import request from '@/utils/request.js'
/**
 * 获取商品分类
 */
export const cateApi = () => {
	return request.get('/category/list')
}