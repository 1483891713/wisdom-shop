import request from '@/utils/request.js';

/**
 * 获取商品详情
 */
export const goodsDetailApi = (goodsId) => {
	return request.get('/goods/detail', {
		goodsId
	});
};
/**
 * 获取购物车商品总数
 */
export const cartTotalApi = () => {
	return request.get('/cart/total');
};
/**
 * 添加商品到购物车
 */
export const addCartApi = (goodsId, goodsNum = 1) => {
	return request.post('/cart/add', {
		goodsId,
		goodsSkuId: '0',
		goodsNum
	});
};

/**
 * 商品保障服务
 */
export const goodsServiceApi = (goodsId) => {
	return request.get('/goods.service/list', {
		goodsId
	});
};

/**
 * 获取商品评论
 */
export const goodsEvaluateApi = (goodsId, limit) => {
	return request.get('/comment/listRows', {
		goodsId,
		limit
	});
};
