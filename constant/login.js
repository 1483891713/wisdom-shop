export const IMAGE_CODE_H5 = 'H5';

export const LOGIN_MOBILE = 'mobile'; //手机号

export const LOGIN_SMSCODE = 'smsCode'; //短信验证码

export const LOGIN_IMGCODE = 'imgCode'; //图片验证码

export const USER_KEY = 'user_key'; //token
